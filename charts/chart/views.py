from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.renderers import JSONRenderer

from chart.serializers import CountriesSerializer
from chart.models import Countries

# Create your views here.
def index(request):
    return render(request,'chart/index.html', {'range': range(20) })

def list_countries(request, category):
    serializer = CountriesSerializer(Countries.objects.filter(region=category).all(), many=True)
    resp = HttpResponse(content_type = 'application/json')
    resp.content = JSONRenderer().render(serializer.data)
    return resp
