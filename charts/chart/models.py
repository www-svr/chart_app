from django.db import models

# Create your models here.
class Countries(models.Model):
    region = models.IntegerField()
    name = models.CharField(max_length=135)
    value = models.IntegerField()
