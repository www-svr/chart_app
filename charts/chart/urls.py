from django.conf.urls import url
from chart import views

urlpatterns = [
    url(r'cat/(\d+)/$', views.list_countries, name='index'),
    url(r'.*$', views.index, name='index'),
]
