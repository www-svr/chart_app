from rest_framework import serializers
from django.contrib.auth.models import User
from chart.models import Countries


class CountriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Countries
        fields = ( 'name', 'value')
