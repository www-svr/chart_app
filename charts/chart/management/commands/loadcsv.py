from django.core.management.base import BaseCommand, CommandError
from chart.models import Countries

import csv
from datetime import datetime
import os
import re

class Command(BaseCommand):
    help = 'Populate database data from csv'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str)

    def handle(self, *args, **options):
        filepath = os.path.abspath(options['file_path'])
        if not os.path.exists(filepath):
            raise CommandError('Please check the file exists: %s' % filepath)
        with open(filepath, 'r') as country_base:
            field_names = ('region', 'name', 'value')
            reader = csv.DictReader(country_base, fieldnames=field_names, delimiter=';')
            for country_row in reader:
                country = Countries(**country_row)
                country.save()
        self.stdout.write(self.style.SUCCESS('Successfully loaded file "%s"' % filepath))
        # for poll_id in options['poll_id']:
        #     try:
        #         poll = Poll.objects.get(pk=poll_id)
        #     except Poll.DoesNotExist:
        #         raise CommandError('Poll "%s" does not exist' % poll_id)
        #
        #     poll.opened = False
        #     poll.save()
        #
        #     self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
