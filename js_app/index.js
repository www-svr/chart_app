var Chart = require('chart.js');
var $ = require('jquery');



function onChangeCategory(){
  var ctx = $("#countryChart");
  var drop_num = $('#catSelect').val();
  $.get('/cat/%s/'.replace('%s', drop_num),function(xx){
    console.log(xx);
    var data = {
      datasets: [{
        data: xx.map(function(item){ return item['value'] }),
        backgroundColor: "#000ba8",
        label: 'population'
      }],

      labels: xx.map(function(item){ return item['name'] }),


    };

    var conf = {
      type: 'bar',
      data: data,
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
      }
    };
    var countryChart = new Chart(ctx, conf);
  });

};

$('#catSelect').change(onChangeCategory);
