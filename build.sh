pip install -r requirements.txt
pushd js_app
npm i
npm run build
popd
pushd charts
python manage.py makemigrations
python manage.py makemigrations chart
python manage.py migrate
python manage.py loadcsv ../list.csv
python manage.py runserver
